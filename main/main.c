#include <stdio.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "driver/uart.h"
#include "esp_log.h"
#include "driver/gpio.h"
#include "esp_system.h"
#include "driver/timer.h"
#include "ds18b20.h"


float temp = 0;
char t[20];

void app_main(void)
{
    gpio_pad_select_gpio(ds18b20_pin);
    while(1)
    {
        temp = temperature();
        sprintf(t, "T = %0.2f °C\n", temp);
        printf("%s", t);
        vTaskDelay(1000/ portTICK_PERIOD_MS);
    }
}
