#ifndef __DS18B20_H
#define __DS18B20_H

#include <stdio.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "driver/uart.h"
#include "esp_log.h"
#include "driver/gpio.h"
#include "esp_system.h"
#include "driver/timer.h"

#define DS18B20_SKIP_ROM					0xCC //gui tin hieu den tat ca Slave
#define DS18B20_CONVERT_T					0x44 // bat dau chuyen doi nhiet do
#define DS18B20_READ_SCRATCH_PAD	0xBE //Doc du lieu tu Slave

#define ds18b20_pin 	4

unsigned char ds18b20_reset();
void ds18b20_write_bit(unsigned char bit);		// ham gui 1 bit vao DS18B20
unsigned char ds18b20_read_bit();							// ham doc 1 bit nhan ve tu DS18B20
void ds18b20_write_byte(unsigned char byte);	// ham gui 1 byte vao DS18B20
unsigned char ds18b20_read_byte();						// ham doc 1 byte nhan ve tu DS18B20
float temperature();

//set pin output
void Set_Pin_Output (gpio_num_t gpio_num)
{
	gpio_set_direction(gpio_num, GPIO_MODE_OUTPUT);
}

//set pin input
void Set_Pin_Input (gpio_num_t gpio_num)
{
	gpio_set_direction(gpio_num, GPIO_MODE_INPUT);
}

// Return 0: OK ; 1:FAIL
unsigned char ds18b20_reset(){
	unsigned char result;
	Set_Pin_Output (ds18b20_pin);
	gpio_set_level(ds18b20_pin, 0);

    ets_delay_us(480);
	gpio_set_level(ds18b20_pin, 1);
	Set_Pin_Input (ds18b20_pin);	
    ets_delay_us(70);

	result = gpio_get_level(ds18b20_pin);
    ets_delay_us(410);

	return result;
}
// ham gui 1 bit vao DS18B20
void ds18b20_write_bit(unsigned char bit){
	if(bit == 1){
	Set_Pin_Output (ds18b20_pin);
	gpio_set_level(ds18b20_pin, 0);

	ets_delay_us(6);

	gpio_set_level(ds18b20_pin, 1);
	ets_delay_us(64);
	}else{

	Set_Pin_Output (ds18b20_pin);
	gpio_set_level(ds18b20_pin, 0);

	ets_delay_us(60);

	gpio_set_level(ds18b20_pin, 1);

	ets_delay_us(10);
	}
}
//ham doc 1 bit nhan ve tu DS18B20
unsigned char ds18b20_read_bit(){
	unsigned char result = 0;

	Set_Pin_Output (ds18b20_pin);
	gpio_set_level(ds18b20_pin, 0);

	ets_delay_us(6);

	gpio_set_level(ds18b20_pin, 1);
	Set_Pin_Input (ds18b20_pin);	
	ets_delay_us(9);

	result = gpio_get_level(ds18b20_pin);

	ets_delay_us(55);
	return result;
}
//ham ghi 1 byte vao DS18B20 (gui bit thap nhat truoc)
void ds18b20_write_byte(unsigned char byte){
	unsigned char i = 8;
	while(i--){
		ds18b20_write_bit(byte & 0x01);		//gui bit co trong so thap nhat (b=1010 1111 & 0000 0001 -> 0000 0001)
		byte >>= 1;												//byte >>=1 -> 0101 0111
	}
}
//ham doc 1 byte nhan ve tu DS18B20
unsigned char ds18b20_read_byte(){
	unsigned char i = 8, result = 0;
	while(i--){
		result >>= 1;
		result |= ds18b20_read_bit()<<7;			//0000 0001 << 7 = 1000 0000
	}
	return result;
}

float temperature(){
	uint8_t tempL, tempH;
	float temp = 0;
	//Ket noi den Slave, bat dau qua trinh chuyen doi nhiet do
	while(ds18b20_reset());
	ds18b20_write_byte(DS18B20_SKIP_ROM);
	ds18b20_write_byte(DS18B20_CONVERT_T);
	vTaskDelay(94/portTICK_PERIOD_MS);   //che do 9 bit mat 93.75ms de chuyen doi

	//Ket noi den Slave, bat dau doc du lieu nhiet do
	while(ds18b20_reset());
	ds18b20_write_byte(DS18B20_SKIP_ROM);
	ds18b20_write_byte(DS18B20_READ_SCRATCH_PAD);

	tempL = ds18b20_read_byte();
	tempH = ds18b20_read_byte();

	temp = ((tempH << 8)+ tempL)*1.0f/16; //lay nhiet do tu temp_Register
	return temp;
}

#endif
